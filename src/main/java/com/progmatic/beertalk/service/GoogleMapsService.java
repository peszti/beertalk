package com.progmatic.beertalk.service;

import com.progmatic.beertalk.model.Coordinate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Bron
 */
@Service
public class GoogleMapsService {

    List<Coordinate> coordinates;

    public GoogleMapsService() {
        this.coordinates = new ArrayList<>();
        Coordinate c1 = new Coordinate(47.493135, 19.070862);
        coordinates.add(c1);
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }
}
