/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.service;


import com.progmatic.beertalk.config.MainWebAppInitializer;
import com.progmatic.beertalk.dto.EventDto;
import com.progmatic.beertalk.dto.EventHasUserDto;
import com.progmatic.beertalk.dto.VisitorOfEventDto;
import com.progmatic.beertalk.model.Address;
import com.progmatic.beertalk.model.Authorities;
import com.progmatic.beertalk.model.Event;
import com.progmatic.beertalk.model.EventHasUser;
import com.progmatic.beertalk.model.EventHasUserPK;
import com.progmatic.beertalk.model.User;
import com.progmatic.beertalk.support.EventDtoGenerator;
import com.progmatic.beertalk.support.SessionBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author BA
 */
@Service
public class EventService {

    private static final Logger LOG = LoggerFactory.getLogger(EventService.class);

    @PersistenceContext
    EntityManager em;

    @Autowired

    SessionBean sessionBean;
    
    @Autowired
    EventDtoGenerator eDtoGenerator;
    
    @Autowired
    private EventDao eventDao;
    
    @Autowired
    private EmailService emailSender;
    
    @Value("${spring.mail.username}")
    private String adminEmail;
    
    @Transactional
    public void createEvent(EventDto e) {
        //TODO put loged in User to event
        Event event=new Event();
        EventHasUser presenter=new EventHasUser();
        User user = new User();
        Address address=new Address();
        address.setAddress(e.getAddress());
        address.setPubName(e.getPubName());
        event.setAddress(address);
        address.setEventList(new ArrayList<>());
        address.getEventList().add(event);
        user.setEmail(e.getPresenterId());
        presenter.setUser(user);
        event.setDescription(e.getDescription());
        event.setLanguage(e.getLanguage());
        event.setMaxAttendance(e.getMaxAttendance());
        event.setDate(e.getDate());
        event.setTitle(e.getTitle()); 
        event.setResignTime(e.getResignTime());
        ArrayList<EventHasUser> eventHasUserList=new ArrayList<>();
        event.setEventHasUserList(eventHasUserList);
        event.setImage(e.getImage());
        
        em.persist(address);
        em.flush();
        em.persist(event);
        em.flush();
        
        EventHasUserPK ehupk = new EventHasUserPK();
        ehupk.setEventid(event.getId());
        ehupk.setUseremail(e.getPresenterId());
        
        presenter.setEventHasUserPK(ehupk);
        presenter.setRoleInEvent(EventHasUser.ROLE_PRESENTER);
        presenter.setDateOfSignup(new Date());

        em.persist(presenter);
        
        LOG.debug("New event created!", e.getTitle());
    }

    public boolean isPictureOK(byte[] img) {
        int picSize = img.length;
        if (picSize > 5*1024*1024) {
            return false;
        }
        return true;
    }

    @Transactional
    public List<User> allPresentators() {
        List<EventHasUser> tempList = em.createNamedQuery("EventHasUser.findByRoleInEvent")
                .setParameter("roleInEvent", EventHasUser.ROLE_PRESENTER)
                .getResultList();
        List<User> presentaterList = new ArrayList<>();
        for (EventHasUser eventHasUser : tempList) {
            presentaterList.add(eventHasUser.getUser());
        }
        return presentaterList;
    }
    
    @Transactional
    public List<EventDto> getFutureDtoEvents() {
        List<Event> futureEvents = getFutureEvents();
        List<EventDto> futureEventDtos = new ArrayList<>();
        for (Event event : futureEvents) {
            EventDto eventDto = eDtoGenerator.getEventDtoFromEvent(event);
            futureEventDtos.add(eventDto);
        }
        return futureEventDtos;
    }
    
    @Transactional
    public List<Event> getFutureEvents() {
        Date actualDate = new Date();
        List<Event> futureEvents = em.createQuery("SELECT e FROM Event e WHERE e.date >= :date ORDER BY date ASC")
                .setParameter("date", actualDate)
                .getResultList();
        return futureEvents;
    }
    
    @Transactional
    public List<EventDto> getPastDtoEvents() {
        List<Event> pastEvents = getPastEvents();
        List<EventDto> pastEventDtos = new ArrayList<>();
        for (Event event : pastEvents) {
            EventDto eventDto = eDtoGenerator.getEventDtoFromEvent(event);
            pastEventDtos.add(eventDto);
        }
        return pastEventDtos;
    }
    
    @Transactional
    public List<Event> getPastEvents() {
        Date actualDate = new Date();
        List<Event> pastEvents = em.createQuery("SELECT e FROM Event e WHERE e.date < :date ORDER BY date DESC")
                .setParameter("date", actualDate)
                .getResultList();
        return pastEvents;
    }

    @Transactional
    public EventDto signUpForEvent(EventHasUserDto ehudto) {
        String userEmail = sessionBean.getLogedInUser().getEmail();
        Integer eventId = ehudto.getEvent().getId();
        
        EventHasUserPK ehupk = new EventHasUserPK();
        ehupk.setEventid(eventId);
        ehupk.setUseremail(userEmail);
        
        EventHasUser ehu = new EventHasUser();
        ehu.setEventHasUserPK(ehupk);
        int freePlaces = eDtoGenerator.getFreePlacesOfEvent(getEventFromDb(eventId));
        if (freePlaces <= 0) {
            ehu.setRoleInEvent(EventHasUser.ROLE_WAITING);
        } else {
            ehu.setRoleInEvent(EventHasUser.ROLE_VISITOR);
        }
        ehu.setDateOfSignup(new Date());
        ehu.setOrderInList(eDtoGenerator.getFullSizeOfEhu(eventId));
        
        em.persist(ehu);
        
        EventDto thisEventDto = eDtoGenerator.getEventDtoFromEvent(getEventFromDb(eventId));
        LOG.error("A logged in user tried to sign for an event!");
       
        String text = "Kedves " + sessionBean.getLogedInUser().getUsername() + "! \n\n"
                + "Feliratkoztál egy beerTalk eseményre : " + thisEventDto.getTitle()
                + "Az esemény föbb adatai a következöek: \n"
                + "Idöpont: " + thisEventDto.getDate() + "\n"
                + "Helyszín: " + thisEventDto.getAddress() + "\n"
                + "Cím: " + thisEventDto.getTitle() + "\n"
                + "Leírás: " + thisEventDto.getDescription() + "\n\n"
                + "bövebb információért látogasd meg a Beertalk honlapját! (beertalk.hu)";

        emailSender.sendMail(userEmail, "Feliratkozás egy BeerTalk eseményre", text);
        
        return thisEventDto;   
    }
    
    @Transactional
    public EventDto resignFromEvent(EventHasUserDto ehudto) {
        String userEmail = sessionBean.getLogedInUser().getEmail();
        Integer eventId = ehudto.getEvent().getId();

        EventHasUser thisEhu = new EventHasUser();
        thisEhu = eDtoGenerator.getEhuFromDb(eventId, userEmail);
        
        List<EventHasUser> waitingList = getWaitingEhusOfEvent(eventId);
        if (waitingList.size() > 0 && thisEhu.getRoleInEvent() == EventHasUser.ROLE_VISITOR) {
            EventHasUser waitingEhu = waitingList.get(0);
            waitingEhu.setRoleInEvent(EventHasUser.ROLE_VISITOR);
            
            String to = waitingEhu.getUser().getEmail();
            String message = "Kedves " + sessionBean.getLogedInUser().getUsername() + "! \n\n"
                + "A várólistáról átkerültél a látogatók listájára a következö beerTalk eseményen: " + thisEhu.getEvent().getTitle();
            emailSender.sendMail(to, "Felkerültél a beerTalk esemény látogató-listájára! :) ", message);
        }
        
        thisEhu.setRoleInEvent(EventHasUser.ROLE_RESIGNED);
        
        String text = "Kedves " + sessionBean.getLogedInUser().getUsername() + "! \n\n"
                + "Leliratkoztál egy beerTalk eseményröl : " + thisEhu.getEvent().getTitle();
        String text2 = "Kedves Admin!\n\n"
                + "Egy felhasználó, " + sessionBean.getLogedInUser().getUsername()
                + " leliratkozott egy beerTalk eseményröl : " + thisEhu.getEvent().getTitle();

        emailSender.sendMail(userEmail, "Leliratkozás egy BeerTalk eseményröl", text);
        
        List<User> adminList = em.createQuery("SELECT a.user FROM Authorities a WHERE a.authority = :authority")
                .setParameter("authority", "ROLE_ADMIN")
                .getResultList();
        for (User user : adminList) {
            String adminsEmail = user.getEmail();
            emailSender.sendMail(adminsEmail, "Leliratkozás egy BeerTalk eseményröl", text2);
        }
        
        //emailSender.sendMail(adminEmail, "Leliratkozás egy BeerTalk eseményröl", text2);
        
        EventDto thisEventDto = eDtoGenerator.getEventDtoFromEvent(getEventFromDb(eventId));
        LOG.error("A logged in user tried to resign from an event!");
        return thisEventDto; 
    }
    
    @Transactional
    public List<EventHasUser> getWaitingEhusOfEvent (Integer eventId) {
        return em.createQuery("SELECT ehu FROM EventHasUser ehu "
                    + "WHERE ehu.eventHasUserPK.eventid = :eventid "
                    + "AND ehu.roleInEvent = :roleInEvent "
                    + "ORDER BY orderInList ASC")
                .setParameter("eventid", eventId)
                .setParameter("roleInEvent", EventHasUser.ROLE_WAITING)
                .getResultList();
    }
    
    @Transactional
    public EventDto signForEventAgain(EventHasUserDto ehudto) {
        String userEmail = sessionBean.getLogedInUser().getEmail();
        Integer eventId = ehudto.getEvent().getId();
        
        EventHasUser ehu = new EventHasUser();
        ehu = eDtoGenerator.getEhuFromDb(eventId, userEmail);
        int freePlaces = eDtoGenerator.getFreePlacesOfEvent(getEventFromDb(eventId));
        if (freePlaces <= 0) {
            ehu.setRoleInEvent(EventHasUser.ROLE_WAITING);
            int lastNumInOrder = getVisitorsOfEvent(eventId).get(getVisitorsOfEvent(eventId).size()-1).getOrderInList();
            ehu.setOrderInList(lastNumInOrder+1);
        } else {
            ehu.setRoleInEvent(EventHasUser.ROLE_VISITOR);
        }
        ehu.setDateOfSignup(new Date());
        
        
        String text = "Kedves " + sessionBean.getLogedInUser().getUsername() + "! \n\n"
                + "Feliratkoztál egy beerTalk eseményre : " + ehu.getEvent().getTitle()
                + "Az esemény föbb adatai a következöek: \n"
                + "Idöpont: " + ehu.getEvent().getDate() + "\n"
                + "Helyszín: " + ehu.getEvent().getAddress() + "\n"
                + "Cím: " + ehu.getEvent().getTitle() + "\n"
                + "Leírás: " + ehu.getEvent().getDescription() + "\n\n"
                + "bövebb információért látogasd meg a Beertalk honlapját! (beertalk.hu)";

        emailSender.sendMail(userEmail, "Feliratkozás egy BeerTalk eseményre", text);

        
        EventDto thisEventDto = eDtoGenerator.getEventDtoFromEvent(getEventFromDb(eventId));
        LOG.error("A logged in user tried to resign from an event!");  
        return thisEventDto;      
    }
    
    @Transactional
    public Event getEventFromDb (Integer eventId) {
        return (Event) em.createQuery("SELECT e FROM Event e "
                    + "WHERE e.id = :eventid ")
                .setParameter("eventid", eventId)
                .getSingleResult();
    }
    
    @Transactional
    public EventDto findEventById(Integer eventId){
        Event e=eventDao.findOne(eventId);
        List<EventHasUser> ehu = e.getEventHasUserList();
        for (EventHasUser eventHasUser : ehu) {
            eventHasUser.getUser();
        }
        EventDto edto=eDtoGenerator.getEventDtoFromEvent(e);
        return edto;
    }
    
    @Transactional
    public Event modifyEvent(EventDto newEventData, Integer eventId, MultipartFile file) throws IOException{        
        Event eventToModify=em.find(Event.class, eventId);
        
        eventToModify.getAddress().setPubName(newEventData.getPubName());
        eventToModify.getAddress().setAddress(newEventData.getAddress());
        eventToModify.setDate(newEventData.getDate());
        eventToModify.setTitle(newEventData.getTitle());
        eventToModify.setDescription(newEventData.getDescription());
        eventToModify.setLanguage(newEventData.getLanguage());
        eventToModify.setMaxAttendance(newEventData.getMaxAttendance());
        eventToModify.setResignTime(newEventData.getResignTime());
        
        eventToModify.setImage(modifyImage(file, eventId));
        
        
        ArrayList<EventHasUser> eventHasUserList=new ArrayList<>();
        eventToModify.setEventHasUserList(eventHasUserList);
        
        EventHasUser presenter=new EventHasUser();
        EventHasUserPK ehupk = new EventHasUserPK();
        ehupk.setEventid(eventToModify.getId());
        ehupk.setUseremail(newEventData.getPresenterId());
        
        presenter.setEventHasUserPK(ehupk);
        presenter.setRoleInEvent(EventHasUser.ROLE_PRESENTER);
        presenter.setDateOfSignup(new Date());
        
        List<EventHasUser> usersOnEvent = getVisitorsOfEvent(eventId);
        for (EventHasUser ehu : usersOnEvent) {
            User user = ehu.getUser();
            String to = user.getEmail();
            String text = "Kedves " + sessionBean.getLogedInUser().getUsername() + "! \n\n"
                    + "Feliratkoztál egy beerTalk eseményre : " + eventToModify.getTitle() + "\n"
                    + "Ezen az eseményen módosítások történtek. Az esemény új adatai a következöek: \n"
                    + "Új idöpont: " + eventToModify.getDate() + "\n"
                    + "Új helyszín: " + eventToModify.getAddress() + "\n"
                    + "Új cím: " + eventToModify.getTitle() + "\n"
                    + "Új leírás: " + eventToModify.getDescription() + "\n\n"
                    + "bövebb információért látogasd meg a Beertalk honlapját! (beertalk.hu)";
            
            emailSender.sendMail(to, "BeerTalk esemény módosítása", text);
        }
        
        em.merge(presenter);
        em.merge(eventToModify);
        
        return eventToModify;
    }
    
    public void deleteEvent(Integer eventId) {
        List<EventHasUser> usersListAtEvent = em.createNamedQuery("EventHasUser.findByEventid")
                .setParameter("eventid", eventId)
                .getResultList();
        Event eventToDelete = (Event) em.createNamedQuery("Event.findById")
                .setParameter("id", eventId)
                .getSingleResult();
        
        for (EventHasUser ehu : usersListAtEvent) {
            User user = ehu.getUser();
            String to = user.getEmail();
            String text = "Kedves " + user.getUsername() + "! \n\n"
                    + "Feliratkoztál egy beerTalk eseményre : " + eventToDelete.getTitle() + "\n"
                    + "Ezt az esemény sajnos töröltük! \n\n"
                    + "Bövebb információért látogasd meg a Beertalk honlapját! (beertalk.hu)";
            
            emailSender.sendMail(to, "BeerTalk esemény törlése", text);
        }
        
        em.createQuery("DELETE FROM EventHasUser e WHERE e.eventHasUserPK.eventid = :eventid")
                .setParameter("eventid", eventId);
        eventDao.delete(eventId);
    }
    
    @Transactional
    public byte[] modifyImage( MultipartFile file, Integer eventId) throws IOException{
        byte[] eventToModify=em.find(Event.class, eventId).getImage();
        
        if(file.getBytes().length==0){
            return eventToModify;
        }else{
            eventToModify=file.getBytes();
        }
        return eventToModify;
    }
    
    @Transactional
    public List<EventHasUser> getVisitorsOfEvent(Integer eventId){
        List<EventHasUser> ehuList = em.createQuery("SELECT ehu FROM EventHasUser ehu "
                    + "WHERE ehu.eventHasUserPK.eventid = :eventid "
                    + "ORDER BY orderInList ASC")
                .setParameter("eventid", eventId)
                .getResultList();
        return ehuList;
    }
    
    @Transactional
    public void setNewOrderToVisitors(VisitorOfEventDto[] visDtoList) {
        for (VisitorOfEventDto visDto : visDtoList) {
            Integer eventId = visDto.getEventId();
            String userEmail = visDto.getUserEmail();
            EventHasUser ehu = eDtoGenerator.getEhuFromDb(eventId, userEmail);
            ehu.setOrderInList(visDto.getRowIndex());
        }
        LOG.debug("A change has been made on order list!");
    }
    
}
