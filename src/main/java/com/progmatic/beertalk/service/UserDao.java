/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.service;

import com.progmatic.beertalk.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Norb
 */
public interface UserDao extends JpaRepository<User, String>{
    
}
