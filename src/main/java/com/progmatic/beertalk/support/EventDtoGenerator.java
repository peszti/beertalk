package com.progmatic.beertalk.support;

import com.progmatic.beertalk.dto.EventDto;
import com.progmatic.beertalk.model.Event;
import com.progmatic.beertalk.model.EventHasUser;
import com.progmatic.beertalk.model.User;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Norb
 */
@Component
@Scope(scopeName = WebApplicationContext.SCOPE_SESSION,
        proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EventDtoGenerator {
    
    @PersistenceContext
    EntityManager em;
    
    @Autowired
    SessionBean sessionBean;
    
    public EventDto getEventDtoFromEvent(Event event) {
        EventDto eventDto= new EventDto();
        
        eventDto.setId(event.getId());
        eventDto.setTitle(event.getTitle());
        eventDto.setDescription(event.getDescription());
        eventDto.setDate(event.getDate());
        eventDto.setMaxAttendance(event.getMaxAttendance());
        eventDto.setImage(event.getImage());
        eventDto.setLanguage(event.getLanguage());
        eventDto.setAddress(event.getAddress().getAddress());
        eventDto.setPubName(event.getAddress().getPubName());
        eventDto.setPresenterId(getPresenterOfEvent(event.getId()).getEmail());
        eventDto.setPresenterName(getPresenterOfEvent(event.getId()).getUsername());
        eventDto.setHoursLeft(getHoursLeftOfEvent(event));
        eventDto.setFreePlacesLeft(getFreePlacesOfEvent(event));
        eventDto.setLoggedInUserRole(getEventUserRole(event.getId()));
        eventDto.setResignTime(event.getResignTime());
        
        return eventDto;
    }
    
    @Transactional
    public User getPresenterOfEvent (Integer eventId) {
        try {
            EventHasUser ehu = (EventHasUser) em.createQuery("SELECT ehu FROM EventHasUser ehu "
                    + "WHERE ehu.eventHasUserPK.eventid = :eventid "
                    + "AND ehu.roleInEvent = :roleInEvent")
                .setParameter("eventid", eventId)
                .setParameter("roleInEvent", EventHasUser.ROLE_PRESENTER)
                .getSingleResult();
            return ehu.getUser();
        } catch(NoResultException e) {
            return null;
        }    
    }
    
    public double getHoursLeftOfEvent (Event event) {
        Date eventDate = event.getDate();
        Date now = new Date();
        double diffInHours = ((double)(eventDate.getTime() - now.getTime()))/1000/60/60;
        return diffInHours;
    }
    
    @Transactional
    public int getFreePlacesOfEvent (Event event) {
        List<EventHasUser> reservedPlaces = em.createQuery("SELECT ehu FROM EventHasUser ehu "
                    + "WHERE ehu.eventHasUserPK.eventid = :eventid "
                    + "AND ehu.roleInEvent = :roleInEvent")
                .setParameter("eventid", event.getId())
                .setParameter("roleInEvent", EventHasUser.ROLE_VISITOR)
                .getResultList();
        int freePlaces = event.getMaxAttendance() - reservedPlaces.size();
        return freePlaces;
    }
    
    @Transactional
    public int getFullSizeOfEhu (Integer eventId) {
        List<EventHasUser> ehuList = em.createQuery("SELECT ehu FROM EventHasUser ehu "
                    + "WHERE ehu.eventHasUserPK.eventid = :eventid ")
                .setParameter("eventid", eventId)
                .getResultList();
        return ehuList.size();
    }
    
    @Transactional
    public short getEventUserRole(Integer eventId) {
        String userEmail = sessionBean.getLogedInUser().getEmail();
        EventHasUser ehu = new EventHasUser();
        ehu = getEhuFromDb(eventId, userEmail);
        if (ehu == null) {
            return 0;
        } else {
            return ehu.getRoleInEvent();
        }
    }
    
    @Transactional
    public EventHasUser getEhuFromDb (Integer eventId, String userEmail) {
        try {
            return (EventHasUser) em.createQuery("SELECT ehu FROM EventHasUser ehu "
                    + "WHERE ehu.eventHasUserPK.eventid = :eventid "
                    + "AND ehu.eventHasUserPK.useremail = :useremail")
                .setParameter("eventid", eventId)
                .setParameter("useremail", userEmail)
                .getSingleResult();
        } catch(NoResultException e) {
            return null;
        }    
    }
    
}
