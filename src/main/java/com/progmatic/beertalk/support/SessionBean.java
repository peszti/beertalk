/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.support;

import com.progmatic.beertalk.model.User;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author MB
 */
@Component
@Scope(scopeName = WebApplicationContext.SCOPE_SESSION,
        proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionBean {

    User user;
    @PersistenceContext
    EntityManager em;

    /**
     * Find the loged in user.
     *
     * @return User the loged in user. null, if the user not loged in.
     */
    public User getLogedInUser() {
        if (user == null) {
            if (SecurityContextHolder.getContext().getAuthentication().getPrincipal()
                    instanceof org.springframework.security.core.userdetails.User) {
                
                org.springframework.security.core.userdetails.User springUser
                        = (org.springframework.security.core.userdetails.User)
                        SecurityContextHolder.getContext()
                            .getAuthentication().getPrincipal();

                
                user = em.find(User.class, springUser.getUsername());

            }
        }
        return user;
    }
}
