package com.progmatic.beertalk.controller;

import com.progmatic.beertalk.model.Coordinate;
import com.progmatic.beertalk.service.GoogleMapsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Bron
 */

@RestController
public class GoogleMapsController {
    @Autowired
    GoogleMapsService ms;

    @CrossOrigin
    @RequestMapping(value = {"/coordinates"}, method = RequestMethod.GET)
    public List<Coordinate> getCoordinates(Model model){

        return ms.getCoordinates();
    }
}
