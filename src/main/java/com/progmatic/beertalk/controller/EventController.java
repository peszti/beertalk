package com.progmatic.beertalk.controller;

import com.progmatic.beertalk.model.EventHasUser;
import com.progmatic.beertalk.dto.EventDto;
import com.progmatic.beertalk.model.User;
import com.progmatic.beertalk.service.EventService;
import com.progmatic.beertalk.service.UserService;
import com.progmatic.beertalk.support.SessionBean;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author BA
 */
@Controller
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private UserService userService;

    @Autowired
    private SessionBean sessionbean;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UserController.class);

    //@author Norbi
    @RequestMapping(value = {"/events", "/"}, method = GET)
    public String home(Model model) {
        model.addAttribute("futureEventList", eventService.getFutureDtoEvents());
        model.addAttribute("pastEventList", eventService.getPastDtoEvents());
        model.addAttribute("role_visitor", EventHasUser.ROLE_VISITOR);
        model.addAttribute("role_presenter", EventHasUser.ROLE_PRESENTER);
        model.addAttribute("role_resigned", EventHasUser.ROLE_RESIGNED);
        model.addAttribute("role_waiting", EventHasUser.ROLE_WAITING);
        model.addAttribute("role_not_signed_up", EventHasUser.ROLE_NOT_SIGNED_UP);
        model.addAttribute("page", "events");
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Események - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "events";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = {"/events/create"}, method = RequestMethod.GET)
    public String createEvent(Model model) {
        model.addAttribute("event", new EventDto());
        model.addAttribute("userList", userService.getAllUsers());
        model.addAttribute("page", "createEvent");
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Esemény létrehozása - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "createEvent";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(path = "/events/create", method = RequestMethod.POST)
    public String createEvent(@Valid EventDto event, @RequestParam("file") MultipartFile file, BindingResult bindingResult) throws IOException {
        LOG.debug("create event started");
//        if(!(eventService.isPictureOK(file.getBytes()))){
//            return "redirect:/error";
//        }
        if (bindingResult.hasErrors()) {
            return "createEvent";
        }
        try {
            event.setImage(file.getBytes());
            
        } catch (IOException ex) {
            Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
        }
        eventService.createEvent(event);
        return "redirect:/events";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = {"events/{id}"}, method = RequestMethod.GET)
    public String getSingleEvent(Model model, @PathVariable("id") Integer id) {
        model.addAttribute("event", eventService.findEventById(id));
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Esemény részletei - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "singleEvent";
    }

    @ResponseBody
    @RequestMapping(value = {"events/image/{id}"}, method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getEventImage(Model model, @PathVariable("id") Integer id) throws IOException {
        EventDto event = eventService.findEventById(id);
        if (event.getImage().length > 0) {
            return event.getImage();
        } else {
            InputStream in = new ClassPathResource("/static/images/Logo_02.png").getInputStream();
            return IOUtils.toByteArray(in);
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = {"/events/{id}/modify"}, method = RequestMethod.GET)
    public String getSingleEventModifyPage(Model model, @PathVariable("id") Integer id) {
        model.addAttribute("event", eventService.findEventById(id));
        model.addAttribute("userList", userService.getAllUsers());
        model.addAttribute("page", "createEvent");
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Esemény módosítása - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "modifyevent";

    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(path = "/events/{id}/modify", method = RequestMethod.POST)
    public String modifyEvent(@PathVariable("id") Integer eventId, Model model, @Valid EventDto event, @RequestParam("file") MultipartFile file, BindingResult bindingResult) throws IOException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("event", event);
            model.addAttribute("userList", userService.getAllUsers());
            event.setImage(file.getBytes());
            return "modifyevent";

        }
        event.setImage(file.getBytes());
        eventService.modifyEvent(event, eventId, file);
        return "redirect:/events/{id}";

    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(path = "/events/{id}/delete", method = RequestMethod.POST)
    public String deleteEvent(@PathVariable("id") Integer eventId) throws IOException {
        eventService.deleteEvent(eventId);
        return "redirect:/events";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = {"events/{id}/visitors"}, method = RequestMethod.GET)
    public String showVisitors(Model model, @PathVariable("id") Integer id) {
        model.addAttribute("eventId", id);
        model.addAttribute("visitors", eventService.getVisitorsOfEvent(id));
        model.addAttribute("role_visitor", EventHasUser.ROLE_VISITOR);
        model.addAttribute("role_presenter", EventHasUser.ROLE_PRESENTER);
        model.addAttribute("role_resigned", EventHasUser.ROLE_RESIGNED);
        model.addAttribute("role_waiting", EventHasUser.ROLE_WAITING);
        model.addAttribute("gender_male", User.GENDER_MALE);
        model.addAttribute("gender_female", User.GENDER_FEMALE);
        model.addAttribute("page", "visitors");
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Eseményre feliratkozók listája - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "visitors";
    }

}
