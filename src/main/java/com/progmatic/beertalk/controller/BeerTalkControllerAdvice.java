/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

/**
 *
 * @author Bron
 */
@ControllerAdvice
public class BeerTalkControllerAdvice {
    
     private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BeerTalkControllerAdvice.class);


    @ExceptionHandler(Exception.class)
    public String handleRestErrors(Exception ex, Model model) {
        LOG.error("unexpected error", ex);
        model.addAttribute("errorText", ex.getMessage());
        return "error";
    }

}
