/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.controller;

import com.progmatic.beertalk.dto.UserDto;
import com.progmatic.beertalk.model.User;
import com.progmatic.beertalk.service.UserService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MB
 */
@RestController
@RequestMapping("rest")
public class UserRestController {
    
    @Autowired
    UserService userServise;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UserService.class);
    
    @RequestMapping(path = "getUserData", method = RequestMethod.POST, 
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public UserDto getUserData(UserDto userDto){
        LOG.debug("Input email: {}", userDto.getEmail());
        return userServise.getOneUserDtoById(userDto.getEmail());
    }
}
