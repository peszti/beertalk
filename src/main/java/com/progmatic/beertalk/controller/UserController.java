/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.progmatic.beertalk.dto.PasswordDto;
import com.progmatic.beertalk.model.Token;
import com.progmatic.beertalk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import com.progmatic.beertalk.dto.UserDto;
import com.progmatic.beertalk.dto.UserTreeDto;
import com.progmatic.beertalk.model.PasswordResetToken;
import com.progmatic.beertalk.service.EmailService;
import com.progmatic.beertalk.model.User;
import com.progmatic.beertalk.service.UserDao;
import java.io.IOException;
import java.io.InputStream;
import com.progmatic.beertalk.support.SessionBean;
import java.util.Calendar;
import java.util.UUID;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author MB
 */
@Controller
public class UserController {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userservice;

    @Autowired
    private EmailService emailSender;
    
    @Autowired
    private UserDao userDao;
    
    @Autowired
    private SessionBean sessionbean;

    /**
     * registrationlink generation page loader by MB
     *
     * @param model
     * @return "getregistrationcode" back to the page
     */
    @RequestMapping(value = "/createregistrationcode", method = RequestMethod.GET)
    public String getRegistrationCode(Model model) {
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Meghivó kérése - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        
        if (userservice.showUserProfile().getWarmBeer() > UserService.warmBeerLevel) {
            model.addAttribute("error", "Jelenleg nem kérhetsz több meghívót!");
        } else {
            model.addAttribute("enableForm", true);
            model.addAttribute("page", "regcode");
        }
        return "getregistrationcode";
    }

    /**
     * registration link generater by MB
     *
     * @param model generated registration link
     * @param request request header param
     * @param tokenLink
     * @return "getregistrationcode" back to the page
     */
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @RequestMapping(value = "/createregistrationcode", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String createRegistrationCode(Model model, HttpServletRequest request, String tokenLink) {
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Regisztrációs kód - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        if (userservice.showUserProfile().getWarmBeer() < UserService.warmBeerLevel) {
            String token = userservice.insertNewTokenToDb();
            tokenLink = String.format("%s://%s:%d/registration/",
                    request.getScheme(),
                    request.getServerName(),
                    request.getServerPort())
                    + token;
            model.addAttribute("registrationlink", tokenLink);
            model.addAttribute("enableForm", true);
        }
        return tokenLink;
    }

    // by PE
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = {"/adminpage"}, method = RequestMethod.GET)
    public String showUsers(Model model) {
        model.addAttribute("users", userservice.getAllUsers());
        model.addAttribute("gender_male", User.GENDER_MALE);
        model.addAttribute("gender_female", User.GENDER_FEMALE);
        model.addAttribute("page", "adminpage");
        // by SG --> Oldalcím
        model.addAttribute("slider", "rangeSlider");
        model.addAttribute("pageTitle", "Regisztrált felhasználók - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "adminpage";
    }

    /**
     * registration page loader by MB
     *
     * @param invitationToken Base64 String
     * @param model registration form
     * @param redirectAttributes
     * @return Sting "registration" html if the token valid else redirect to
     * errorPage.html
     */
    @RequestMapping(value = "/registration/{invitationToken}", method = RequestMethod.GET)
    public String registrationPage(@PathVariable("invitationToken") String invitationToken,
            Model model,
            RedirectAttributes redirectAttributes) {
        LOG.debug("Somebody try to registrate with token: {}", invitationToken);
        Token token = userservice.findToken(invitationToken);
        if (token != null && token.getIsValid() == Token.NOT_USED) {
            model.addAttribute("UserCandidate", new UserDto());
            model.addAttribute("invitationToken", invitationToken);
            model.addAttribute("page", "registration");
            return "registration";
        } else {
            throw new RuntimeException("A meghívó érvénytelen!");
        }
    }

    @RequestMapping(value = "/registration/{invitationToken}", method = RequestMethod.POST)
    public String registrationProcess(@PathVariable("invitationToken") String invitationToken,
            Model model, UserDto candidate, HttpServletRequest request,
            @ModelAttribute("success") String success,
            RedirectAttributes redirectAttributes) {
        User user = userservice.registration(candidate, invitationToken);
        if (user != null) {
            String text = String.format("%s://%s:%d/registration/activate/",
                    request.getScheme(),
                    request.getServerName(),
                    request.getServerPort())
                    + user.getActivationCode();
            emailSender.sendMail(user.getEmail(), "BeerTalk: regisztráció megerösítése", text);
            success = "Regisztráció sikeres! Nézd meg a postafiókodat, hogy aktiválhasd a felhasználódat!";
            redirectAttributes.addFlashAttribute("success", success);
            return "redirect:/login";
        } else {
            throw new RuntimeException("Regisztráció sikertelen!");
        }
    }

    @RequestMapping(value = "/registration/activate/{activationCode}", method = RequestMethod.GET)
    public String activateRegistration(@PathVariable("activationCode") String activationCode,
            Model model,
            @ModelAttribute("success") String success,
            RedirectAttributes redirectAttributes) {
            userservice.activateRegistration(activationCode);
            //TODO: wright to the user: the activation activation has been successful
            success = "Aktiváció sikeres. Mostmár bejelentkezhetsz.";
            redirectAttributes.addFlashAttribute("success", success);
            return "redirect:/login";
        
    }

    // by PE
    @RequestMapping(value = "/userprofile", method = RequestMethod.GET)
    public String showUserProfile(Model model) {
        model.addAttribute("user", userservice.showUserProfile());
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Profil - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        model.addAttribute("gender_male", User.GENDER_MALE);
        model.addAttribute("gender_female", User.GENDER_FEMALE);
        return "userprofile";
    }
    

    @ResponseBody
    @RequestMapping(value="/userprofile/image", method=RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getUserProfilePicture(Model model) throws IOException{
        User user=sessionbean.getLogedInUser();
        if (user.getImage().length > 0) {
            return user.getImage();
        } else {
            InputStream in=new ClassPathResource("/static/images/logo_icon1.png").getInputStream();
            return IOUtils.toByteArray(in);
        }
    }
    
    // by PE
    @RequestMapping(value = "/userprofile/modify", method = RequestMethod.GET)
    public String modifyUserProfile(Model model) {
        User u = userservice.showUserProfile();
        UserDto uc = new UserDto();
        uc.setGender(u.getGender());
        uc.setUsername(u.getUsername());
        uc.setBirthDate(u.getBirthDate());
        uc.setPhoneNumber(u.getPhoneNumber());
        uc.setEmail(u.getEmail());
        uc.setImage(u.getImage());
        model.addAttribute("user", uc);
        model.addAttribute("page", "modifyuserdata");
        model.addAttribute("gender_male", User.GENDER_MALE);
        model.addAttribute("gender_female", User.GENDER_FEMALE);
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Profil módosítása - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "modifyuserdata";
    }

    @RequestMapping(value = "/userprofile/modify/image", method = RequestMethod.POST, produces = MediaType.IMAGE_JPEG_VALUE)
    public String modifyUserProfilePicture(Model model, @RequestParam("file") MultipartFile file) throws IOException {
        User loggedInUser=sessionbean.getLogedInUser();
        //loggedInUser.setImage(userservice.modifyProfilePic(file));
        userservice.setUserProfilePic(file);
        return "redirect:/userprofile";
    }
    
    // by PE
    @RequestMapping(value = "/userprofile/modify", method = RequestMethod.POST)
    public String modifyUserProfile(Model model, @Valid UserDto user,
            BindingResult bindingResult) throws IOException {
        if (bindingResult.hasErrors()) {
            LOG.debug("there were errors during valdation of the user object");
            model.addAttribute("user", user);
            return "modifyuserdata";
        }
        userservice.modifyUserProfile(user);
        return "redirect:/userprofile";
    }

    //by PE
    @RequestMapping(value = "/userprofile/changepw", method = RequestMethod.GET)
    public String showChangePw(Model model) {
        PasswordDto passwordNew = new PasswordDto();
        model.addAttribute("passwordNew", passwordNew);
        model.addAttribute("page", "change_pw");
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Jelszó módosítása - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "change_pw";
    }

    //by PE
    @RequestMapping(value = "/userprofile/changepw", method = RequestMethod.POST)
    public String changePw(Model model, @Valid PasswordDto pw,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            LOG.error("There were errors during valdation of the password object");
            model.addAttribute("passwordNew", pw);
            return "change_pw";
        } else {
            if (userservice.changePw(pw) == null) {
                return "redirect:/userprofile/changepw";
            } else {
                return "redirect:/userprofile";
            }
            
        }
    }

    //by PE
    @RequestMapping(value = "/createPwResetToken", method = RequestMethod.POST)

    public String createPwResetToken(Model model, HttpServletRequest request, @RequestParam("email") String userEmail) {
        LOG.debug("createPwResetToken started");
        User user = userservice.findUserByEmail(userEmail);
        if (user == null) {
            LOG.error("user not found: {}", userEmail);
            //         throw new UserNotFoundException();
        }
        String token = UUID.randomUUID().toString();
        LOG.debug("Token:{}", token);
        userservice.createResetPwTokenForUser(user, token);
        String pwResetTokenLink = String.format("%s://%s:%d/updatePw/" + token,
                request.getScheme(),
                request.getServerName(),
                request.getServerPort());
        userservice.sendPasswordResetEmail(pwResetTokenLink, user);
        
        return "redirect:/pwResetEmailSent";
    }

    //by PE
    @RequestMapping(value = "/pwResetEmailSent", method = RequestMethod.GET)
    public String sendpwResetEmail() {
        return "pwResetEmailSent";
    }

    // by PE
    @RequestMapping(value = "/updatePw/{pwResetToken}", method = RequestMethod.GET)
    public String updatePw(@PathVariable("pwResetToken") String pwResetToken,
            Model model,
            RedirectAttributes redirectAttributes) {
        LOG.debug("Somebody try to change Pw with pwResetToken: {}", pwResetToken);
        PasswordResetToken token = userservice.findPwResetToken(pwResetToken);

        Calendar cal = Calendar.getInstance();
        if (token != null && (token.getExpiryDate().after(cal.getTime()))) {
            model.addAttribute("passwordDto", new PasswordDto());
            model.addAttribute("pwResetToken", pwResetToken);
            return "updatePw";
        } else {
            throw new RuntimeException("Érvénytelen visszaállító kód!");
        }
    }

    //by PE
    @RequestMapping(value = "/updatePw/{pwResetToken}", method = RequestMethod.POST)
    public String updatePw(@PathVariable("pwResetToken") String pwResetToken,
            Model model, UserDto userDto, HttpServletRequest request, PasswordDto pw,
            @ModelAttribute("success") String success,
            RedirectAttributes redirectAttributes) {
        PasswordResetToken token = userservice.findPwResetToken(pwResetToken);
        if (token.getIsValid() == PasswordResetToken.NOT_USED) {
            User user = token.getUser();
            if (user != null) {
                userservice.changePwForUser(pw, user);
                token.setIsValid(PasswordResetToken.USED);
                success = "A jelszó módosítás sikeres volt!";
                redirectAttributes.addFlashAttribute("success", success);
                return "redirect:/login";
            } else {
                throw new RuntimeException("Érvénytelen visszaállító kód!");
            }
        } else {
            LOG.warn("Token: {} is used, password reset is unsuccessful!", token);
            throw new RuntimeException("Valami hiba lépett fel a jelszómódosítás során!");
        }
    }

    // by Zoli
    @RequestMapping(value = "/usertree", method = RequestMethod.GET)
    public String createUserTree(Model model) throws JsonProcessingException {
        User u = userservice.showUserProfile();
        User rootUser = userservice.getRootUser(u);
        UserTreeDto fullTree = new UserTreeDto();
        UserTreeDto tree = userservice.buildTreeFromRoot(rootUser, fullTree);
        String treeInJSON = userservice.convertTreeToJSON(tree);
        model.addAttribute("treeInJSON", treeInJSON);
        model.addAttribute("pageTitle", "Profil - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "usertree";
    }
}
