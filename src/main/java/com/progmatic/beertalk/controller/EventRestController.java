package com.progmatic.beertalk.controller;


import com.progmatic.beertalk.dto.EventDto;
import com.progmatic.beertalk.dto.EventHasUserDto;
import com.progmatic.beertalk.dto.VisitorOfEventDto;
import com.progmatic.beertalk.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Norb
 */
@RestController
@RequestMapping("rest")
public class EventRestController {

    @Autowired
    private EventService eventService;
     
    //@author Norbi
    @RequestMapping(path = "signUpForEvent", method = RequestMethod.POST, 
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public Integer signUpForEvent(EventHasUserDto ehudto){
        EventDto edto = eventService.signUpForEvent(ehudto);
        return edto.getFreePlacesLeft();
    }
    
    @RequestMapping(path = "resignFromEvent", method = RequestMethod.PUT, 
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public Integer resignFromEvent(EventHasUserDto ehudto){
        EventDto edto = eventService.resignFromEvent(ehudto);
        return edto.getFreePlacesLeft();
    }
   
    @RequestMapping(path = "signForEventAgain", method = RequestMethod.PUT, 
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public Integer signForEventAgain(EventHasUserDto ehudto){
        EventDto edto = eventService.signForEventAgain(ehudto);
        return edto.getFreePlacesLeft();
    }
    
    @RequestMapping(path = "saveVisitorList", method = RequestMethod.PUT, 
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String saveVisitorList(@RequestBody VisitorOfEventDto[] visDtoArray){
        eventService.setNewOrderToVisitors(visDtoArray);
        return "ok";
    }
}
