/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.controller;

import javax.servlet.http.HttpServletResponse;
import com.progmatic.beertalk.support.SessionBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author MB
 */
@Controller
public class HomeController {
    
    @Autowired
    private SessionBean sessionbean;
    
    @RequestMapping(value = "/errorpage", method = RequestMethod.GET)
    public String getErrorPage(
    @ModelAttribute("error") String errorText,
    @ModelAttribute("error2") String errorNumber,
            HttpServletResponse httpRequest,Model model){
         
        int httpErrorCode = (Integer) httpRequest.getStatus();
        switch (httpErrorCode) {
            case 400: {
                errorNumber = "400";
                errorText = "Bad Request";
                break;
            }
            case 403: {
                errorNumber = "403";
                errorNumber = "Forbidden";
                break;
            }
            case 404: {
                errorNumber = "404";
                errorNumber = "Resource not found";
                break;
            }
            case 500: {
                errorNumber = "500";
                errorNumber = "Internal Server Error";
                break;
            }
        }
        model.addAttribute("errorText", errorText);
        model.addAttribute("errorNumber", errorNumber);
        return "error";
    }
    //NA
    @RequestMapping(value = "/gallery", method = RequestMethod.GET)
    public String gallery(Model model) {
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Fényképek - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "gallery";
    }
    //NA
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String contact(Model model) {
        model.addAttribute("page", "contact");
        // by SG --> Oldalcím
        model.addAttribute("pageTitle", "Kapcsolat - Beertalk");
        model.addAttribute("username", sessionbean.getLogedInUser().getUsername());
        return "contact";
    }
}
