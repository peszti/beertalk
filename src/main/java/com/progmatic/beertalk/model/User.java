/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.model;

import com.progmatic.beertalk.utils.DateConstants;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author User
 */
@Entity
@Table(name = "users")
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
    , @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email")
    , @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username")
    , @NamedQuery(name = "User.findByPhoneNumber", query = "SELECT u FROM User u WHERE u.phoneNumber = :phoneNumber")
    , @NamedQuery(name = "User.findByBirthDate", query = "SELECT u FROM User u WHERE u.birthDate = :birthDate")
    , @NamedQuery(name = "User.findByGender", query = "SELECT u FROM User u WHERE u.gender = :gender")
    , @NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password")
    , @NamedQuery(name = "User.findByDescription", query = "SELECT u FROM User u WHERE u.description = :description")
    , @NamedQuery(name = "User.findByInvitedBy", query = "SELECT u FROM User u WHERE u.invitedBy = :invitedBy")
    , @NamedQuery(name = "User.findByDateOfReg", query = "SELECT u FROM User u WHERE u.dateOfReg = :dateOfReg")
    , @NamedQuery(name = "User.findByEnabled", query = "SELECT u FROM User u WHERE u.enabled = :enabled")
    , @NamedQuery(name = "User.findByActivationCode", query = "SELECT u FROM User u WHERE u.activationCode = :activationCode")})
public class User implements Serializable {
    
    public static final short DEACTIVE = 0;
    public static final short ACTIVE = 1;
    
    public static final short GENDER_FEMALE = 2;
    public static final short GENDER_MALE = 1;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userEmail")
    private List<Token> tokenList;

    private static final long serialVersionUID = 1L;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "phone_number")
    private String phoneNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "birth_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    @DateTimeFormat(pattern=DateConstants.DATE_PATTERN)
    private Date birthDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gender")
    private short gender;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "password")
    private String password;
    @Size(max = 400)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enabled")
    private short enabled;
    @Column(name = "warm_beer")
    private int warmBeer;
    @Column(name = "activation_code")
    private String activationCode;
    @ManyToOne
    private User invitedBy;
    @Basic(optional = false)
    @Column(name = "dateOfReg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfReg;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<EventHasUser> eventHasUserList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<Authorities> authoritiesList;
    @Lob
    @Column(name = "image")
    private byte[] image;
    
    public User() {
    }

    public User(String email) {
        this.email = email;
    }

    public User(String email, String username, String phoneNumber, 
            Date birthDate, short gender, String password, 
            short enabled, int warmBeer, String activationCode, User invitedBy, Date dateOfReg) {
        this.email = email;
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.gender = gender;
        this.password = password;
        this.enabled = enabled;
        this.warmBeer = warmBeer;
        this.activationCode = activationCode;
        this.invitedBy = invitedBy;
        this.dateOfReg = dateOfReg;
    }

    public User getInvitedBy() {
        return invitedBy;
    }

    public void setInvitedBy(User invitedBy) {
        this.invitedBy = invitedBy;
    }

    public Date getDateOfReg() {
        return dateOfReg;
    }

    public void setDateOfReg(Date dateOfReg) {
        this.dateOfReg = dateOfReg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public short getGender() {
        return gender;
    }

    public void setGender(short gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getEnabled() {
        return enabled;
    }

    public void setEnabled(short enabled) {
        this.enabled = enabled;
    }

    public int getWarmBeer() {
        return warmBeer;
    }

    public void setWarmBeer(int warmBeer) {
        this.warmBeer = warmBeer;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationToken) {
        this.activationCode = activationToken;
    }

    public List<EventHasUser> getEventHasUserList() {
        return eventHasUserList;
    }

    public void setEventHasUserList(List<EventHasUser> eventHasUserList) {
        this.eventHasUserList = eventHasUserList;
    }

    public List<Authorities> getAuthoritiesList() {
        return authoritiesList;
    }

    public void setAuthoritiesList(List<Authorities> authoritiesList) {
        this.authoritiesList = authoritiesList;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (email != null ? email.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.email == null && other.email != null) || (this.email != null && !this.email.equals(other.email))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.progmatic.beertalk.model.User[ email=" + email + " ]";
    }

    public List<Token> getTokenList() {
        return tokenList;
    }

    public void setTokenList(List<Token> tokenList) {
        this.tokenList = tokenList;
    }
    
}
