package com.progmatic.beertalk.model;
/**
 *
 * @author Bron
 */
public class Coordinate {
    double lat;
    double lng;

    public Coordinate(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
