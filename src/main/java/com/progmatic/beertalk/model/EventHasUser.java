/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author User
 */
@Entity
@Table(name = "event_has_user")
@NamedQueries({
    @NamedQuery(name = "EventHasUser.findAll", query = "SELECT e FROM EventHasUser e")
    , @NamedQuery(name = "EventHasUser.findByEventid", query = "SELECT e FROM EventHasUser e WHERE e.eventHasUserPK.eventid = :eventid")
    , @NamedQuery(name = "EventHasUser.findByUseremail", query = "SELECT e FROM EventHasUser e WHERE e.eventHasUserPK.useremail = :useremail")
    , @NamedQuery(name = "EventHasUser.findByRoleInEvent", query = "SELECT e FROM EventHasUser e WHERE e.roleInEvent = :roleInEvent")})
public class EventHasUser implements Serializable {
    
    public static final short ROLE_NOT_SIGNED_UP = 0;
    public static final short ROLE_VISITOR = 1;
    public static final short ROLE_PRESENTER = 2;
    public static final short ROLE_VIP = 3;
    public static final short ROLE_WAITING = 4;
    public static final short ROLE_RESIGNED = 5;
    
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EventHasUserPK eventHasUserPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "role_in_event")
    private short roleInEvent;
    @JoinColumn(name = "Event_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Event event;
    @JoinColumn(name = "User_email", referencedColumnName = "email", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;
    @Basic(optional = false)
    @Column(name = "dateOfSignup")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfSignup;
    @Basic(optional = false)
    @NotNull
    @Column(name = "order_in_list")
    private int orderInList;

    public EventHasUser() {
    }

    public EventHasUser(EventHasUserPK eventHasUserPK) {
        this.eventHasUserPK = eventHasUserPK;
    }

    public EventHasUser(EventHasUserPK eventHasUserPK, short roleInEvent) {
        this.eventHasUserPK = eventHasUserPK;
        this.roleInEvent = roleInEvent;
    }

    public EventHasUser(int eventid, String useremail) {
        this.eventHasUserPK = new EventHasUserPK(eventid, useremail);
    }

    public EventHasUserPK getEventHasUserPK() {
        return eventHasUserPK;
    }

    public void setEventHasUserPK(EventHasUserPK eventHasUserPK) {
        this.eventHasUserPK = eventHasUserPK;
    }

    public short getRoleInEvent() {
        return roleInEvent;
    }

    public void setRoleInEvent(short roleInEvent) {
        this.roleInEvent = roleInEvent;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDateOfSignup() {
        return dateOfSignup;
    }

    public void setDateOfSignup(Date dateOfSignup) {
        this.dateOfSignup = dateOfSignup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventHasUserPK != null ? eventHasUserPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventHasUser)) {
            return false;
        }
        EventHasUser other = (EventHasUser) object;
        if ((this.eventHasUserPK == null && other.eventHasUserPK != null) || (this.eventHasUserPK != null && !this.eventHasUserPK.equals(other.eventHasUserPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.progmatic.beertalk.model.EventHasUser[ eventHasUserPK=" + eventHasUserPK + " ]";
    }

    public int getOrderInList() {
        return orderInList;
    }

    public void setOrderInList(int orderInList) {
        this.orderInList = orderInList;
    }
    
}
