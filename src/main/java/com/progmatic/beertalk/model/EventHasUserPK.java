/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author User
 */
@Embeddable
public class EventHasUserPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Event_id")
    private int eventid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "User_email")
    private String useremail;

    public EventHasUserPK() {
    }

    public EventHasUserPK(int eventid, String useremail) {
        this.eventid = eventid;
        this.useremail = useremail;
    }

    public int getEventid() {
        return eventid;
    }

    public void setEventid(int eventid) {
        this.eventid = eventid;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventid;
        hash += (useremail != null ? useremail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventHasUserPK)) {
            return false;
        }
        EventHasUserPK other = (EventHasUserPK) object;
        if (this.eventid != other.eventid) {
            return false;
        }
        if ((this.useremail == null && other.useremail != null) || (this.useremail != null && !this.useremail.equals(other.useremail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.progmatic.beertalk.model.EventHasUserPK[ eventid=" + eventid + ", useremail=" + useremail + " ]";
    }
    
}
