/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author PE
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "PasswordResetToken.findAll", query = "SELECT p FROM PasswordResetToken p")
    , @NamedQuery(name = "PasswordResetToken.findByPasswordResetToken", query = "SELECT p FROM PasswordResetToken p WHERE p.passwordResetToken = :passwordResetToken")})
public class PasswordResetToken implements Serializable {
    
    public static final short NOT_USED = 0;
    public static final short USED = 1;
    private static final int EXPIRATION = 60 * 24;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "passwordResetToken")
    private String passwordResetToken;
    @Basic(optional = false)
    
    @Column(name = "isValid")
    private short isValid;
    @JoinColumn(name = "user_email", referencedColumnName = "email")
    @ManyToOne(optional = false)
    private User user;
    
    private Date expiryDate;
    
    public PasswordResetToken() {
    }

    public PasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public PasswordResetToken(String passwordResetToken, User user) {
        this.passwordResetToken = passwordResetToken;
        this.user = user;
    }

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public short getIsValid() {
        return isValid;
    }

    public void setIsValid(short isValid) {
        this.isValid = isValid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return "PasswordResetToken{" + "passwordResetToken=" + passwordResetToken + '}';
    }
    
    
}
