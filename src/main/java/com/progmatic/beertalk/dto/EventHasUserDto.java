package com.progmatic.beertalk.dto;

import com.progmatic.beertalk.model.Event;
import com.progmatic.beertalk.model.EventHasUserPK;
import com.progmatic.beertalk.model.User;

/**
 *
 * @author Norb
 */
public class EventHasUserDto {
    public static final short ROLE_VISITOR = 1;
    public static final short ROLE_PRESENTATOR = 2;
    public static final short ROLE_VIP = 3;
    public static final short ROLE_WAITING = 4;
    public static final short ROLE_RESIGNED = 5;
    
    protected EventHasUserPK eventHasUserPK;
    private short roleInEvent;
    private Event event;
    private User user;
    private int orderInList;

    public EventHasUserDto() {
    }

    public EventHasUserDto(EventHasUserPK eventHasUserPK) {
        this.eventHasUserPK = eventHasUserPK;
    }

    public EventHasUserDto(EventHasUserPK eventHasUserPK, short roleInEvent) {
        this.eventHasUserPK = eventHasUserPK;
        this.roleInEvent = roleInEvent;
    }

    public EventHasUserDto(int eventid, String useremail) {
        this.eventHasUserPK = new EventHasUserPK(eventid, useremail);
    }

    public EventHasUserPK getEventHasUserPK() {
        return eventHasUserPK;
    }

    public void setEventHasUserPK(EventHasUserPK eventHasUserPK) {
        this.eventHasUserPK = eventHasUserPK;
    }

    public short getRoleInEvent() {
        return roleInEvent;
    }

    public void setRoleInEvent(short roleInEvent) {
        this.roleInEvent = roleInEvent;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getOrderInList() {
        return orderInList;
    }

    public void setOrderInList(int orderInList) {
        this.orderInList = orderInList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventHasUserPK != null ? eventHasUserPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventHasUserDto)) {
            return false;
        }
        EventHasUserDto other = (EventHasUserDto) object;
        if ((this.eventHasUserPK == null && other.eventHasUserPK != null) || (this.eventHasUserPK != null && !this.eventHasUserPK.equals(other.eventHasUserPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.progmatic.beertalk.model.EventHasUser[ eventHasUserPK=" + eventHasUserPK + " ]";
    }
}
