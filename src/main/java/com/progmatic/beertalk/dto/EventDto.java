package com.progmatic.beertalk.dto;

import com.progmatic.beertalk.utils.DateConstants;
import java.util.Date;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author BA
 */
public class EventDto {
       
    private Integer id;
    @NotNull
    private String title;
    @NotNull
    private String description;
    @NotNull
    @DateTimeFormat(pattern=DateConstants.DATETIME_PATTERN)
    private Date date;
    @NotNull
    private int maxAttendance;
    private byte[] image;
    @NotNull
    private String language;
    @NotNull
    private String address;
    @NotNull
    private String pubName;
   
    
    private String presenterId;
    private String presenterName;
    private double hoursLeft;
    private int freePlacesLeft;
    private short loggedInUserRole;
    private int resignTime;

    public EventDto() {
    }

    public EventDto(Integer id) {
        this.id=id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMaxAttendance() {
        return maxAttendance;
    }

    public void setMaxAttendance(int maxAttendance) {
        this.maxAttendance = maxAttendance;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPresenterId() {
        return presenterId;
    }

    public void setPresenterId(String presenterId) {
        this.presenterId = presenterId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPubName() {
        return pubName;
    }

    public void setPubName(String pubName) {
        this.pubName = pubName;
    }

    public double getHoursLeft() {
        return hoursLeft;
    }

    public void setHoursLeft(double hoursLeft) {
        this.hoursLeft = hoursLeft;
    }

    public int getFreePlacesLeft() {
        return freePlacesLeft;
    }

    public void setFreePlacesLeft(int freePlacesLeft) {
        this.freePlacesLeft = freePlacesLeft;
    }

    public short getLoggedInUserRole() {
        return loggedInUserRole;
    }

    public void setLoggedInUserRole(short loggedInUserRole) {
        this.loggedInUserRole = loggedInUserRole;
    }

    public Integer getId() {
        return id;
    }

    public String getPresenterName() {
        return presenterName;
    }

    public void setPresenterName(String presenterName) {
        this.presenterName = presenterName;
    }

    public int getResignTime() {
        return resignTime;
    }

    public void setResignTime(int resignTime) {
        this.resignTime = resignTime;
    }
    
    
    
}
