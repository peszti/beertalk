/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class UserTreeDto {
    
    private String username;
    private List<UserTreeDto> children = new ArrayList<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<UserTreeDto> getChildren() {
        return children;
    }

    public void setChildren(List<UserTreeDto> children) {
        this.children = children;
    }
    
    
}
