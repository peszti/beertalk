/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.beertalk.dto;

import com.progmatic.beertalk.utils.DateConstants;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * @Zoli
 */
public class UserDto implements Serializable {
    
    private String email;
    private String username;
    private String phoneNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "birth_date")
    @Temporal(javax.persistence.TemporalType.DATE)
    @DateTimeFormat(pattern=DateConstants.DATE_PATTERN)
    private Date birthDate;
    private Short gender;
    private String password;
    private String password2;
    private byte[] image;
    private String autority;
    private String description;
    private short enabled;
    private int warmBeer;

    public UserDto(String email, String username, String phoneNumber,
            Date birthDate, Short gender, byte[] image, String autority,
            String description, short enabled, int warmBeer) {
        this.email = email;
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.gender = gender;
        this.image = image;
        this.autority = autority;
        this.description = description;
        this.enabled = enabled;
        this.warmBeer = warmBeer;
    }

    public UserDto() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getEnabled() {
        return enabled;
    }

    public void setEnabled(short enabled) {
        this.enabled = enabled;
    }

    public int getWarmBeer() {
        return warmBeer;
    }

    public void setWarmBeer(int warmBeer) {
        this.warmBeer = warmBeer;
    }

    
    
    public String getAutority() {
        return autority;
    }

    public void setAutority(String autority) {
        this.autority = autority;
    }
    
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    
    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Short getGender() {
        return gender;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    public void setGender(Short gender) {
        this.gender = gender;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
