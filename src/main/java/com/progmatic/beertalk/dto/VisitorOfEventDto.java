package com.progmatic.beertalk.dto;


/**
 *
 * @author Norb
 */
public class VisitorOfEventDto {
    private int rowIndex;
    private int eventId;
    private String userEmail;

    public VisitorOfEventDto() {
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

}
