$(function() {
    $(".visitors").tableDnD();

    //save visitor list's order

    $('body').on('click', '.save-visitors', function(){
        var $saveButton = $(this);
        var rowsArray = [];
        $saveButton.siblings('table').find('.visitor-row').each(function () {
            var rowIndex = $(this).index();
            var userEmail = $(this).find('.user-email').text();
            var eventId = parseInt($(this).find('.event-id').text());
            var visitorData = {
                    'rowIndex' : rowIndex,
                    'userEmail' : userEmail,
                    'eventId' : eventId
            };
            rowsArray.push(visitorData);
        });

        $.ajax({
            url: '/rest/saveVisitorList',
            method : 'PUT',
            data : JSON.stringify(rowsArray),
            contentType: 'application/json',
            
            headers: {'X-CSRF-Token': $('#csrf-token input').val()},
            error : function(){
                alert('A mentés sikertelen');
            },
            success : function(){
                alert('A mentés sikeres!');
            }
        });




    });
});
