$(function(){
    $('#datetimepicker').datetimepicker({});

    $('.registration').mouseenter(function (e) {
        e.preventDefault();
        if (validateName() && validateEmail() && validatePhone()
                && validateBirthDate() && validateGender() && validatePassword()
                && validatePassword2() && validateRobot()) {
            $('.registration-btn').prop('disabled', false);
        } else {
            $('.registration-btn').prop('disabled', true);
        }
    });

    $('#name').focusout(function () {
        validateName();
    });

    $('#email').focusout(function () {
        validateEmail();
    });

    $('#phone').focusout(function () {
        validatePhone();
    });

    $('#birthDate').focusout(function () {
        validateBirthDate();
    });

    $('#gender').focusout(function () {
        validateGender();
    });

    $('#password').focusout(function () {
        validatePassword();
    });

    $('#password2').focusout(function () {
        validatePassword2();
    });

    $('#not-robot').focusout(function () {
        validateRobot();
    });

    //validation of name
    function validateName() {
        var name = $('#name').val();
        if (name.length >= 3 && name.length <= 100) {
            $('#name').removeClass('error-validation')
                .addClass('success-validation');
            $('.name-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#name').removeClass('success-validation')
                .addClass('error-validation');
            $('.name-error').text('A névnek 3 és 100 karakter között kell lennie!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of email
    function validateEmail() {
        var email = $('#email').val();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(email).toLowerCase())) {
            $('#email').removeClass('error-validation')
                .addClass('success-validation');
            $('.email-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#email').removeClass('success-validation')
                .addClass('error-validation');
            $('.email-error').text('Helytelenül megadott email cím!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of phone number
    function validatePhone() {
        var phone = $('#phone').val();
        var re = /^\+?\d+([\- \/]*\d){5,}$/;
        if (re.test(phone)) {
            $('#phone').removeClass('error-validation')
                .addClass('success-validation');
            $('.phone-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#phone').removeClass('success-validation')
                .addClass('error-validation');
            $('.phone-error').text('Helytelenül megadott telefonszám!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of birth date
    function validateBirthDate() {
        var birthDate = $('#birthDate').val();
        if (birthDate) {
            $('#birthDate').removeClass('error-validation')
                .addClass('success-validation');
            $('.birthDate-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#birthDate').removeClass('success-validation')
                .addClass('error-validation');
            $('.birthDate-error').text('A születési dátumot kötelező megadni!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of gender
    function validateGender() {
        var gender = $('#gender').val();
        if (gender) {
            $('#gender').removeClass('error-validation')
                .addClass('success-validation');
            $('.gender-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#gender').removeClass('success-validation')
                .addClass('error-validation');
            $('.gender-error').text('A nemet kötelező megadni!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of password
    function validatePassword() {
        var password = $('#password').val();
        var re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
        if (re.test(String(password).toLowerCase())) {
            $('#password').removeClass('error-validation')
                .addClass('success-validation');
            $('.password-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#password').removeClass('success-validation')
                .addClass('error-validation');
            $('.password-error').text('A jelszónak legalább 6 karakternek kell lennie, és tartalmaznia kell betűt és számot is!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of password2
    function validatePassword2() {
        var password = $('#password').val();
        var password2 = $('#password2').val();
        if (password === password2) {
            $('#password2').removeClass('error-validation')
                .addClass('success-validation');
            $('.password2-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#password2').removeClass('success-validation')
                .addClass('error-validation');
            $('.password2-error').text('A két jelszó nem egyezik!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of robot check
    function validateRobot() {
        var notRobot = $('#not-robot');
        if (notRobot.is(':checked')) {
            $('#not-robot').removeClass('error-validation')
                .addClass('success-validation');
            $('.not-robot-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#not-robot').removeClass('success-validation')
                .addClass('error-validation');
            $('.not-robot-error').text('Pipáld ki, ha nem vagy robot!')
                .css('color', 'red');
            return false;
        }
    }
});
