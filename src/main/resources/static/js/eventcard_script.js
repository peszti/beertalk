$(function() {
    //collapsing text to 300 char and back
    var showChar = 300;
    var textBorder = "...";
    var moretext = "Mutass többet! >";
    var lesstext = "Mutass kevesebbet!";

    $('body').find('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var firstPartOfText = content.substr(0, showChar);
            var secondPartOfText = content.substr(showChar, content.length - showChar);

            var html = firstPartOfText
                        + '<span class="text-border">' + textBorder + '</span>'
                        + '<span class="morecontent">'
                            + '<span>' + secondPartOfText + '</span>'
                            + '<a href="" class="morelink">' + moretext + '</a>'
                        + '</span>';

            $(this).html(html);
        }
    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

    //to sign up for event
    $('body').on('click', '.event-signup', function(){
        var $eventSignUp = $(this);
        var eventId = parseInt($eventSignUp.parents('.card').find('#event-id').text());
        var eventUserData = {
                'event.id' : eventId
        };
        $('#verification').modal('show').find('.verification-text').text("Biztosan fel szeretnél iratkozni?");
        $('.verify-yes').click(function () {
            $.ajax({
                    url: '/rest/signUpForEvent',
                    method : 'POST',
                    data : eventUserData,
                    dataType : 'json',
                    headers: {'X-CSRF-Token': $('#csrf-token input').val()},
                    error : function(){
                        alert("A feliratkozás sikertelen!");
                    },
                    success : function(response){
                        if (response > 0) {
                            $eventSignUp.parents('.card').find('.free-places').text(response);
                            $eventSignUp.parents('.card').find('.role-visitor').removeClass('d-none');
                        } else {
                            $eventSignUp.parents('.card').find('.free-places').text("Már csak várólistán van hely!");
                            $eventSignUp.parents('.card').find('.role-waiting').removeClass('d-none');
                        }
                        $eventSignUp.addClass('d-none');
                        $eventSignUp.siblings('.event-resign').removeClass('d-none');
                        $eventSignUp.parents('.card').find('.role-not-sign-up').addClass('d-none');
                        $('#verification').modal('hide');
                    }
            });
        });
    });

    //to resign from event
    $('body').on('click', '.event-resign', function(){
        var $eventResign = $(this);
        var eventId = parseInt($eventResign.parents('.card').find('#event-id').text());
        var eventUserData = {
                'event.id' : eventId
        };
        $('#verification').modal('show').find('.verification-text').text("Biztosan le szeretnél iratkozni?");
        $('.verify-yes').click(function () {
            $.ajax({
                    url: '/rest/resignFromEvent',
                    method : 'PUT',
                    data : eventUserData,
                    dataType : 'json',
                    headers: {'X-CSRF-Token': $('#csrf-token input').val()},
                    error : function(){
                        alert("A leiratkozás sikertelen!");
                    },
                    success : function(response){
                        if (response > 0) {
                            $eventResign.parents('.card').find('.free-places').text(response);

                        } else {
                            $eventResign.parents('.card').find('.free-places').text("Már csak várólistán van hely!");
                            $eventResign.parents('.card').find('.role-waiting').addClass('d-none');
                        }
                        if (!$eventResign.parents('.card').find('.role-visitor').hasClass('d-none')) {
                            $eventResign.parents('.card').find('.role-visitor').addClass('d-none');
                        }
                        if (!$eventResign.parents('.card').find('.role-waiting').hasClass('d-none')) {
                            $eventResign.parents('.card').find('.role-waiting').addClass('d-none');
                        }
                        $eventResign.addClass('d-none');
                        $eventResign.siblings('.event-sign-again').removeClass('d-none');
                        $eventResign.parents('.card').find('.role-resigned').removeClass('d-none');
                        $('#verification').modal('hide');
                    }
            });
        });
    });

    //to sign up for event again when resigned
    $('body').on('click', '.event-sign-again', function(){
        var $eventSignAgain = $(this);
        var eventId = parseInt($eventSignAgain.parents('.card').find('#event-id').text());
        var eventUserData = {
                'event.id' : eventId
        };
        $('#verification').modal('show').find('.verification-text').text("Biztosan fel szeretnél iratkozni?");
        $('.verify-yes').click(function () {
            $.ajax({
                    url: '/rest/signForEventAgain',
                    method : 'PUT',
                    data : eventUserData,
                    dataType : 'json',
                    headers: {'X-CSRF-Token': $('#csrf-token input').val()},
                    error : function(){
                        alert("A feliratkozás sikertelen!");
                    },
                    success : function(response){
                        if (response > 0) {
                            $eventSignAgain.parents('.card').find('.free-places').text(response);
                            $eventSignAgain.parents('.card').find('.role-visitor').removeClass('d-none');
                        } else {
                            $eventSignAgain.parents('.card').find('.free-places').text("Már csak várólistán van hely!");
                            $eventSignAgain.parents('.card').find('.role-waiting').removeClass('d-none');
                        }
                        $eventSignAgain.addClass('d-none');
                        $eventSignAgain.siblings('.event-resign').removeClass('d-none');
                        $eventSignAgain.parents('.card').find('.role-resigned').addClass('d-none');
                        $('#verification').modal('hide');
                    }
            });
        });
    });

    //search for dates in eventlist:
    $('body').find('.search-event').click(function () {
        var dateFrom = new Date($('body').find('.date-from').val()).getTime();
        var dateTo = new Date($('body').find('.date-to').val()).getTime();
        $('body').find('.card').each(function () {
            var $thisEvent = $(this);
            var eventDate = new Date($thisEvent.find('.search-event-date').text()).getTime();
            if ( eventDate < dateFrom || eventDate > dateTo ) {
                $thisEvent.addClass('d-none');
            } else {
                $thisEvent.removeClass('d-none');
            }
        });
    });

});
