 $(function () {
    $('#datetimepicker').datetimepicker({});

    $("#upload-picture-input").change(function(e){
        var fullPath = $(this).val();
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }
        $("#upload-picture-name").text(filename);
    });
    $("#upload-picture-label").click(function(e){
        e.preventDefault();
        $("#upload-picture-input").click();
    });

    $('.add-event, .modify-event').mouseenter(function (e) {
        e.preventDefault();
        if (validateTitle() && validateDescription() && validateMaxAttendance()
                && validateResignTime() && validateAddress() && validateLocation()) {
            $('.add-new-event').prop('disabled', false);
        } else {
            $('.add-new-event').prop('disabled', true);
        }
    });

    $('#title').focusout(function () {
        validateTitle();
    });

    $('#event-description').focusout(function () {
        validateDescription();
    });

    $('#numOfParticipants').focusout(function () {
        validateMaxAttendance();
    });

    $('.event-date').focusout(function () {
        validateDate();
    });

    $('#resignTime').focusout(function () {
        validateResignTime();
    });

    $('#address').focusout(function () {
        validateAddress();
    });

    $('#location').focusout(function () {
        validateLocation();
    });

    //validation of title
    function validateTitle() {
        var title = $('#title').val();
        if (title.length >= 3 && title.length <= 100) {
            $('#title').removeClass('error-validation')
                .addClass('success-validation');
            $('.title-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#title').removeClass('success-validation')
                .addClass('error-validation');
            $('.title-error').text('A címnek 3 és 100 karakter között kell lennie!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of description
    function validateDescription() {
        var description = $('#event-description').val();
        if (description.length >= 3 && description.length <= 10000) {
            $('#event-description').removeClass('error-validation')
                .addClass('success-validation');
            $('.description-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#event-description').removeClass('success-validation')
                .addClass('error-validation');
            $('.description-error').text('A leírásnak 3 és 10000 karakter között kell lennie!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of max attendance
    function validateMaxAttendance() {
        var maxAttendance = $('#numOfParticipants').val();
        var re = /^([0-9]+)$/;
        if (re.test(maxAttendance)) {
            $('#numOfParticipants').removeClass('error-validation')
                .addClass('success-validation');
            $('.participants-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#numOfParticipants').removeClass('success-validation')
                .addClass('error-validation');
            $('.participants-error').text('A max létszámhoz egész számot kell megadni!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of event date
    function validateDate() {
        var date = $('.event-date').val();
        if (date) {
            $('.event-date').removeClass('error-validation')
                .addClass('success-validation');
            $('.event-date-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('.event-date').removeClass('success-validation')
                .addClass('error-validation');
            $('.event-date-error').text('A dátum mezőt kötelező megadni!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of resign time
    function validateResignTime() {
        var resignTime = $('#resignTime').val();
        var re = /^([0-9]+)$/;
        if (re.test(resignTime)) {
            $('#resignTime').removeClass('error-validation')
                .addClass('success-validation');
            $('.resigntime-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#resignTime').removeClass('success-validation')
                .addClass('error-validation');
            $('.resigntime-error').text('A leiratkozási időhöz egész számot kell megadni!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of address
    function validateAddress() {
        var address = $('#address').val();
        if (address.length >= 3 && address.length <= 300) {
            $('#address').removeClass('error-validation')
                .addClass('success-validation');
            $('.address-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#address').removeClass('success-validation')
                .addClass('error-validation');
            $('.address-error').text('A helyszín címének 3 és 300 karakter között kell lennie!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of location
    function validateLocation() {
        var location = $('#location').val();
        if (location.length >= 3 && location.length <= 300) {
            $('#location').removeClass('error-validation')
                .addClass('success-validation');
            $('.location-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#location').removeClass('success-validation')
                .addClass('error-validation');
            $('.location-error').text('A helyszín nevének 3 és 300 karakter között kell lennie!')
                .css('color', 'red');
            return false;
        }
    }

});
