$(function () {
    $('.modify-password').mouseenter(function (e) {
        e.preventDefault();
        if (validateOldPassword() && validatePassword() && validatePassword2()) {
            $('#submitBtn').prop('disabled', false);
        } else {
            $('#submitBtn').prop('disabled', true);
        }
    });

    $('#old-password').focusout(function () {
        validateOldPassword();
    });

    $('#password').focusout(function () {
        validatePassword();
    });

    $('#password2').focusout(function () {
        validatePassword2();
    });

    //validation of old password
    function validateOldPassword() {
        var oldPassword = $('#old-password').val();
        if (oldPassword) {
            $('#old-password').removeClass('error-validation')
                .addClass('success-validation');
            $('.old-password-error').text('');
            return true;
        } else {
            $('#old-password').removeClass('success-validation')
                .addClass('error-validation');
            $('.old-password-error').text('A változtatáshoz meg kell adni a régi jelszót!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of password
    function validatePassword() {
        var password = $('#password').val();
        var re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
        if (re.test(String(password).toLowerCase())) {
            $('#password').removeClass('error-validation')
                .addClass('success-validation');
            $('.password-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#password').removeClass('success-validation')
                .addClass('error-validation');
            $('.password-error').text('A jelszónak legalább 6 karakternek kell lennie, és tartalmaznia kell betűt és számot is!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of password2
    function validatePassword2() {
        var password = $('#password').val();
        var password2 = $('#password2').val();
        if (password === password2) {
            $('#password2').removeClass('error-validation')
                .addClass('success-validation');
            $('.password2-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#password2').removeClass('success-validation')
                .addClass('error-validation');
            $('.password2-error').text('A két jelszó nem egyezik!')
                .css('color', 'red');
            return false;
        }
    }
});
