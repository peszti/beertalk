$(function () {
    $('.datetimepicker').datetimepicker({
    	format:'Y/m/d H:i'
    });
    $('.datepicker').datetimepicker({
     	timepicker:false,
    	format:'Y/m/d'
    });
});
