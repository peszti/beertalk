$(function (){

    var coordinates = [{lat: 47.493135, lng: 19.070862, title: "Krak'n Town", html: '<div><h3>Krak\'n Town</h3><img src="/images/kraknTown.jpg" style="width:200px; height: auto;"/></div>'}];
    initMap();
    var map;
    function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: coordinates[0].lat, lng: coordinates[0].lng, title: "asd"},
                zoom: 15
            });

            $.each(coordinates, function(index, coordinate){
                var cord = {lat: coordinate.lat, lng: coordinate.lng};
                var marker = new google.maps.Marker({
                  position: cord,
                  map: map,
                  title: coordinate.title
                });

                var infowindow = new google.maps.InfoWindow({
                    content: coordinate.html
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            });
        }
});
