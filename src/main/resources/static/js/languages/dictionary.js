$(function () {
	$('.birthDate').text(lang.birthDate);
	$('.cancelSingUpForEvent').text(lang.cancelSingUpForEvent);
	$('.checkThis').text(lang.checkThis);
	$('.createEvent').text(lang.createEvent);
	$('.description').text(lang.description);
	$('.email').text(lang.email);
	$('.enabled').text(lang.enabled);
	$('.enterCorrectBirthDate').text(lang.enterCorrectBirthDate);
	$('.enterCorrectEmail').text(lang.enterCorrectEmail);
	$('.enterCorrectName').text(lang.enterCorrectName);
	$('.enterCorrectTel').text(lang.enterCorrectTel);
	$('.eg-e-mail').text(lang.eg-e-mail);
	$('.eg-date').text(lang.eg-date);
	$('.eg-tel').text(lang.eg-tel);
	$('.eg-name').text(lang.eg-name);
	$('.eventDescription:').text(lang.eventDescription);
	$('.eg-date').text(lang.eg-date);
	$('.eg-password').text(lang.eg-password);
	$('.eventLanguage').text(lang.eventLanguage);
	$('.enterCorrectPassword').text(lang.enterCorrectPassword);
	$('.eventLanguage:').text(lang.eventLanguage);
	$('.eg-passwordAgain').text(lang.eg-passwordAgain);
	$('.eventDescription').text(lang.eventDescription);
	$('.events').text(lang.events);
	$('.enterCorrectPasswordAgain').text(lang.enterCorrectPasswordAgain);
	$('.eng').text(lang.eng);
	$('.freePlaces').text(lang.freePlaces);
	$('.gender').text(lang.gender);
	$('.hun').text(lang.hun);
	$('.invalidLoginData').text(lang.invalidLoginData);
	$('.login').text(lang.login);
	$('.location').text(lang.location);
	$('.location:').text(lang.location2);
	$('.locationName').text(lang.locationName);
	$('.locationName:').text(lang.locationName2);
	$('.logout').text(lang.logout);
	$('.login-btn').text(lang.login-btn);
	$('.registration').text(lang.registration);
	$('.name').text(lang.name);
	$('.yourGender').text(lang.yourGender);
	$('.fremale').text(lang.fremale);
	$('.male').text(lang.male);
	$('.password').text(lang.password);
	$('.passwordAgain').text(lang.passwordAgain);
	$('.IamNotARobot').text(lang.IamNotARobot);
	$('.sendForm').text(lang.sendForm);
	$('.username').text(lang.username);
	$('.invitedBy').text(lang.invitedBy);
	$('.presenter').text(lang.presenter);
	$('.presenter2').text(lang.presenter2);
	$('.maxMember').text(lang.maxMember);
	$('.maxMember2').text(lang.maxMember2);
	$('.tel').text(lang.tel);
	$('.time').text(lang.time);
	$('.time2').text(lang.time2);
	$('.title').text(lang.title);
	$('.title2').text(lang.title2);
	$('.saveEvent').text(lang.saveEvent);
	$('.singUpForEvent').text(lang.singUpForEvent);
	$('.singUpForEventAgain').text(lang.singUpForEventAgain);
	$('.regCodeText').text(lang.regCodeText);
	$('.regCodeGeneration').text(lang.regCodeGeneration);
	$('.showMore').text(lang.showMore);
	$('.showLess').text(lang.showLess);
});