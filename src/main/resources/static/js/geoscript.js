$(function() {
  var apiKey = "AIzaSyCF60qCOfKY9zYLiMWTJtO8vXnXILbq88A";
	var map;
  $('#map-view').click(function() {
    var address = $('#map-view').text();
        console.log(address);

    $.get("https://maps.googleapis.com/maps/api/geocode/json",
    { address: address,
        key: apiKey },
      function(response) {
        if (response.status === "OK") {
					map=new google.maps.Map(document.getElementById('map'), {
	          zoom: 16,
	          center: response.results[0].geometry.location
	        });
					map.setCenter(response.results[0].geometry.location);
						var marker = new google.maps.Marker({
								map: map,
								position: response.results[0].geometry.location
						});
						marker.setMap('map');
						map.panTo(marker.position);
        } else if(response.status !== "OK"){
          	$('#myModal').modal('show');
        }
      }
    );
  });
});
