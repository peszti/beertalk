$(function () {
    $('#copy-code').click(function () {
        /* Get the text field */
        var copyText = document.getElementById("registrationlink");

        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("Copy");
    });

    //get registration code:
    $('body').on('click', '#create-token', function(){
        var $createButton = $(this);
        $.ajax({
                url: '/createregistrationcode',
                method : 'POST',
                data : '{}',
                dataType : 'text',
                contentType : 'application/json',
                headers: {
                    'X-CSRF-Token': $('#csrf-token input').val(),
                },
                error : function(){
                    alert("A kérés sikertelen!");
                },
                success : function(response){
                    $createButton.siblings('#registrationlink').val(response);
                }
        });
    });


});
