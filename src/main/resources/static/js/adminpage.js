$(function (){
	$('body').find('.user-row').click(function(){
		var userId = $(this).children('.user-id').text();
		user = {'email' : userId};
		$.ajax({
            url: '/rest/getUserData',
            method : 'POST',
            data : user,
            dataType: 'json',
            
            headers: {'X-CSRF-Token': $('input#csrf-token').val()},
            error : function(){
            	alert('Something went wrong!');
            },
            success : function(response){
            	$('body').find('#modal-image').attr('src', response.image);
            	$('body').find('#modal-username').text(response.username);
            	$('body').find('#modal-role').text(response.autority);
            	$('body').find('#modal-email').text(response.email);
            	$('body').find('#modal-phoneNumber').text(response.phoneNumber);
            	$('body').find('#modal-birthDate').text(response.birthDate);
            	$('body').find('#modal-gender').text(response.gender);
            	$('body').find('#range_02').text(response.warmBeer);
            	$('body').find('#modal-description').text(response.description);
            }
        });
	});

});