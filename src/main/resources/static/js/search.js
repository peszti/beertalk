function searchName() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("demo-foo-addrow");
    tr = table.getElementsByTagName("tr");
	
	for (i = 0; i < tr.length; i++) {
        td1 = tr[i].getElementsByTagName("td")[0];
		td2 = tr[i].getElementsByTagName("td")[1];
		td3 = tr[i].getElementsByTagName("td")[4];
		
        if (td1) {
            var name = td1.getElementsByTagName("p")[0].innerHTML;
			var email = td2.innerText;
			var description = td3.innerText;

			if (name.toUpperCase().indexOf(filter) > -1 
				|| email.toUpperCase().indexOf(filter) > -1
				|| description.toUpperCase().indexOf(filter) > -1) {
				
				tr[i].style.display = "";
			
			} else {
				tr[i].style.display = "none";
			}		
        }       
    }
}


