$(function(){

    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove':  'Remove',
            'error':   'Ooops, something wrong appended.'
        }
    });

    $('.birthdate').datetimepicker({
        timepicker:false,
        format:'Y/m/d'
    });

    $('.modify-user').mouseenter(function (e) {
        e.preventDefault();
        if (validateName() && validateEmail() && validatePhone()
                && validateBirthDate() && validateGender()) {
            $('.modify-btn').prop('disabled', false);
        } else {
            $('.modify-btn').prop('disabled', true);
        }
    });

    $('#name').focusout(function () {
        validateName();
    });

    $('#email').focusout(function () {
        validateEmail();
    });

    $('#phone').focusout(function () {
        validatePhone();
    });

    $('#birthDate').focusout(function () {
        validateBirthDate();
    });

    $('#gender').focusout(function () {
        validateGender();
    });

    //validation of name
    function validateName() {
        var name = $('#name').val();
        if (name.length >= 3 && name.length <= 100) {
            $('#name').removeClass('error-validation')
                .addClass('success-validation');
            $('.name-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#name').removeClass('success-validation')
                .addClass('error-validation');
            $('.name-error').text('A névnek 3 és 100 karakter között kell lennie!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of email
    function validateEmail() {
        var email = $('#email').val();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(email).toLowerCase())) {
            $('#email').removeClass('error-validation')
                .addClass('success-validation');
            $('.email-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#email').removeClass('success-validation')
                .addClass('error-validation');
            $('.email-error').text('Helytelenül megadott email cím!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of phone number
    function validatePhone() {
        var phone = $('#phone').val();
        var re = /^\+?\d+([\- \/]*\d){5,}$/;
        if (re.test(phone)) {
            $('#phone').removeClass('error-validation')
                .addClass('success-validation');
            $('.phone-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#phone').removeClass('success-validation')
                .addClass('error-validation');
            $('.phone-error').text('Helytelenül megadott telefonszám!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of birth date
    function validateBirthDate() {
        var birthDate = $('#birthDate').val();
        if (birthDate) {
            $('#birthDate').removeClass('error-validation')
                .addClass('success-validation');
            $('.birthDate-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#birthDate').removeClass('success-validation')
                .addClass('error-validation');
            $('.birthDate-error').text('A születési dátumot kötelező megadni!')
                .css('color', 'red');
            return false;
        }
    }

    //validation of gender
    function validateGender() {
        var gender = $('#gender').val();
        if (gender) {
            $('#gender').removeClass('error-validation')
                .addClass('success-validation');
            $('.gender-error').text('Jónak tűnik!')
                .css('color', 'green');
            return true;
        } else {
            $('#gender').removeClass('success-validation')
                .addClass('error-validation');
            $('.gender-error').text('A nemet kötelező megadni!')
                .css('color', 'red');
            return false;
        }
    }

});
