$(function (){

  $(function() {
      $(".preloader").fadeOut();
  });
  $(function() {
      $('[data-toggle="tooltip"]').tooltip()
  });
  // ==============================================================
  // Login and Recover Password
  // ==============================================================
  $('#to-recover').on("click", function() {
      $("#loginform").slideUp();
      $("#recoverform").fadeIn();
  });

  $('.new-password').mouseenter(function (e) {
      e.preventDefault();
      if (validateEmail()) {
          $('.new-password-button').prop('disabled', false);
      } else {
          $('.new-password-button').prop('disabled', true);
      }
  });

  $('#email').focusout(function () {
      validateEmail();
  });

  //validation of email
  function validateEmail() {
      var email = $('#email').val();
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (re.test(String(email).toLowerCase())) {
          $('#email').removeClass('error-validation')
              .addClass('success-validation');
          $('.email-error').text('Jónak tűnik!')
              .css('color', 'green');
          return true;
      } else {
          $('#email').removeClass('success-validation')
              .addClass('error-validation');
          $('.email-error').text('Helytelenül megadott email cím!')
              .css('color', 'red');
          return false;
      }
  }

});
